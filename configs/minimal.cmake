# ------ A list of options that could be activated during cmake call -----
option(WITH_FFTW "Look for and use fftw in the softwarew, default = OFF" OFF)
option(WITH_BOOST "Look for and use boost, default = OFF" OFF)
option(WITH_DOCUMENTATION "Generate documentation. Default = OFF" OFF)
option(WITH_TESTS "Activate tests. Default = OFF" OFF)
set(CMAKE_BUILD_TYPE Release CACHE STRING "Build mode")
