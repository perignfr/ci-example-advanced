# ------ A list of options that could be activated during cmake call -----
option(WITH_FFTW "Look for and use fftw in the softwarew, default = OFF" ON)
option(WITH_BOOST "Look for and use boost, default = OFF" ON)
option(WITH_DOCUMENTATION "Generate documentation. Default = OFF" OFF)
option(WITH_TESTS "Activate tests. Default = OFF" ON)
set(CMAKE_BUILD_TYPE Debug CACHE STRING "Build mode")
