"""An example of python package with doc

"""


class someclass(object):
    """Some class doing something
    """

    def __init__(self, obstacles, coeff=None):
        """

        Parameters
        ----------
        obstacles : list
            first param
        coeff : double, optional
            second optional param

        Notes
        -----
        blabla

        """
        # the code
        self.var1 = obstacles
        self.var2 = coeff
        truc = 12

        print(f'build example with v1={self.var1} and v2={self.var2}')
