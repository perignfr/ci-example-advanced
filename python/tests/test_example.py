
from mydemo import example


def test_1():
    a = example.someclass(1)
    assert a.var1 == 1
    assert a.var2 is None


def test_2():
    a = example.someclass(2, 4)
    assert a.var1 == 2
    assert a.var2 == 4

def test_3():
    a = example.someclass(2, 8)
    assert a.var1 == 2
    assert a.var2 == 8
