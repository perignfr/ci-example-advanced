# Templates

- demo2.1: ubuntu pipeline, before_script, needs and deps

- demo2.2: CI variables, debian, ubuntu, ubuntu-lite

- demo2.3 + tmpl2.3: templates and reports.

- demo2.4 + tmpl2.4: build docker images with rules

- demo2.5 + tmpl2.5: rules to handle a specific workflow

- demo2.6 + tmpl2.6: add install template REQ CI_DEPLOY_USER et CI_DEPLOY_PASSWORD

- demo2.7 : release

- demo2.8 triggered jobs (local and cross-project)
