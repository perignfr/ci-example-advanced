# CED trainings for cmake
#
#

# ---- CMake global configuration --
# Set the minimal required version
cmake_minimum_required(VERSION 3.19)

# The directory which contains "user-defined" macros and functions
# some tools for cmake that are specific to your project
# For instance, where to find MyTools.cmake file included just below.
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
include(MyTools)

# Default option file name (else read from command line, -DCONF_FILE=...)
# Examples available in configs
set(CONF_FILE ${CMAKE_SOURCE_DIR}/configs/default.cmake CACHE PATH "config file")

# Check for tilde in file name (not handled properly by cmake)
string(FIND ${CONF_FILE} "\~" res)
if(res EQUAL 0)
  string(REPLACE "\~" "$ENV{HOME}" CONF_FILE ${CONF_FILE})
endif()
if(NOT IS_ABSOLUTE ${CONF_FILE})
  get_filename_component(CONF_FILE ${CONF_FILE} REALPATH
    BASE_DIR ${CMAKE_CURRENT_BINARY_DIR})
endif()
message("\n !!!!! Load user-defined options set from file ${CONF_FILE} !!!!! \n")
# Configure to take into account any change in ${CONF_FILE}
configure_file(${CONF_FILE} current_options.cmake COPYONLY)
include(${CMAKE_CURRENT_BINARY_DIR}/current_options.cmake)


# Good practice : be explicit with the build type you expect
# (release, debug ...)
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release")
  # --> if CMAKE_BUILD_TYPE is not set during cmake call, then
  # the behavior will be "release" mode.

endif()

# ------ Project setup ------

# Name of the project and required languages
project(democi CXX C Fortran)
set(CMAKE_CXX_STANDARD 17)

# - Fortran users only : define where modules will be generated
set(CMAKE_Fortran_MODULE_DIRECTORY ${CMAKE_BINARY_DIR}/Modules)

# Reminder : we want to create
# 1 - a library tools based on source files from the directory src/tools
# 2 - a binary, based on src/main2.cpp and using the library tools
# 3 - optionnaly add some extra functionnalities in the software, based on boost and fftw 

# 1 - Declare and create the library

# first of all, we need the list of directories that contain source files for
# the library
set(SRC_DIRS ${CMAKE_SOURCE_DIR}/src/tools/)
# Then this list of directories is parsed to collect source files names
# Notice that any new file in src/tools will be automatically taken into
# account.
get_sources(VAR proj_SRCS DIRS ${SRC_DIRS})

# Then, the library is created based on the list of source files.
add_library(tools SHARED ${proj_SRCS})
# include (-I) path needed to compile the library
target_include_directories(tools PUBLIC ${SRC_DIRS})
target_include_directories(tools PUBLIC $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}>)

# 2 - optional components
# Do we need boost ?
# Yes if cmake has been called with -DWITH_BOOST=ON
if(WITH_BOOST)
  # If so, we
  # - look for boost on the system
  set(Boost_NO_BOOST_CMAKE 1)
  set(boost_min_version 1.75) # This is the minimum for c++20 compatibility of ublas.
  find_package(Boost 1.65 REQUIRED)
  # - turn HAS_BOOST to true (this will be used in config.hpp)
  set(HAS_BOOST 1)
  # - add new include path to our target (library tools)
  # to take into accound headers using boost.
  target_link_libraries(tools PUBLIC Boost::boost)
endif()

# Do we need fftw ?
# Almost the same job as for boost
if(WITH_FFTW)
  # look for fftw ...
  find_package(fftw3 REQUIRED COMPONENTS fftw3)

  # add some extra source files, those from src/tools/fft directory 
  target_sources(tools PRIVATE ${CMAKE_SOURCE_DIR}/src/tools/fft/fftw.f90)
  target_sources(tools PRIVATE ${CMAKE_SOURCE_DIR}/src/tools/fft/fftw3_basicusage.c)
  # notice that we mix up C and fortran files

  # add some extra include path
  target_include_directories(tools PUBLIC src/tools/fft)
  # and link our library with fftw libs
  target_link_libraries(tools PRIVATE fftw3::fftw3 )
  #target_include_directories(tools PRIVATE ${FFTW_INCLUDE_DIR})
  set(HAS_FFTW 1) # for config.hpp
endif()


# --- Generate tools_config.hpp ---
configure_file(config.hpp.cmake tools_config.hpp)

# --- step : create a binary using the library created just above
# binary name and source files
add_executable(runner src/main2.cpp)
# link the binary with libtools
target_link_libraries(runner PRIVATE tools)

# ---- Final step : install the end-user components
# i.e. what will be required to use the software
# (no sources or config files :
# just the library and the binary)
install(TARGETS runner tools
RUNTIME DESTINATION bin # executables
ARCHIVE DESTINATION lib # librairies statiques
LIBRARY DESTINATION lib # librairies dynamiques
)

# Generate (option) doc
if(WITH_DOCUMENTATION)
  add_subdirectory(docs)
endif()

# ---- Test examples ---
if(WITH_TESTS)
  enable_testing()
  if(HAS_BOOST)
    add_executable(test_boost src/tests/test_boost.cpp)
    target_link_libraries(test_boost PUBLIC tools)
    add_test(testBoost ${CMAKE_BINARY_DIR}/test_boost)
  endif()
  if(HAS_FFTW)
    add_executable(test_fft src/tests/test_fft.f90)
    target_link_libraries(test_fft PUBLIC tools)
    add_test(testFFT ${CMAKE_BINARY_DIR}/test_fft)
  endif()
endif()

#---- Summary ----
message(STATUS "====================== Summary ======================")
message(STATUS " Fortran compiler : ${CMAKE_Fortran_COMPILER}")
message(STATUS " CXX compiler : ${CMAKE_CXX_COMPILER}")
if(HAS_BOOST)
  message(STATUS " Boost found in : ${Boost_INCLUDE_DIRS}")
else()
  message(STATUS " Boost not found.")
endif()
message(STATUS " Sources are in : ${CMAKE_SOURCE_DIR}")
message(STATUS " Project will be installed in ${CMAKE_INSTALL_PREFIX}")
message(STATUS " CMake build type is : ${CMAKE_BUILD_TYPE}")

