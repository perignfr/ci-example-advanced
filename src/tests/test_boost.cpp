#include "tools_interface.hpp"
#include <iostream>
#ifdef HAS_BOOST
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#endif

int main(void)
{
  int info = 1;
  #ifdef HAS_BOOST

  boost::numeric::ublas::matrix<double> m (3, 3);
  // Si tout se passe bien info = 0
  if (m.size1() == 3)
  {
    info = 0;
  }
  #endif
  return info;
  
}
