#include "tools_interface.hpp"
#include <iostream>
#ifdef HAS_BOOST
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
using namespace boost::numeric::ublas;
#endif


int main(int argc, char* argv[])
{

  std::cout << "Appel du programme main\n";

  tools myvar;
  myvar.set_tools(1,2);
  myvar.affiche();

  #ifdef HAS_BOOST
  matrix<double> m (3, 3);
  for (unsigned i = 0; i < m.size1 (); ++ i)
    for (unsigned j = 0; j < m.size2 (); ++ j)
      m (i, j) = 3 * i + j;
  std::cout << m << std::endl;
  std::cout << "Boost is on ...\n";
  #else
  std::cout << "Boost is off ...\n";
  #endif 
  #ifdef HAS_FFTW
  std::cout << "FFTW is on ...\n";
  apply_fftw();
  #ifdef FLOAT_PREC
  apply_fftwf();
  #endif
  #else
  std::cout << "FFTW is off ...\n";
  #endif
}
