#ifndef TOOLS_INTERFACE_HPP
#define TOOLS_INTERFACE_HPP
#include "tools_config.hpp"
#include "tools.hpp"

// Ce fichier sert d'interface aux différentes dépendances.
// Le contenu de tools_config.hpp est utilisé pour vérifier
// si la dépendance est disponible et si c'est le cas
// inclure les headers correspondants :

// fftw
#ifdef HAS_FFTW
#include "fftw_tools.hpp"
#endif

#endif
