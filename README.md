# Mise en place CI/CD pour un code de calcul

<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:12px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:12px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"> <img style="height:12px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

TEST PLM 

Démo/training en lien avec le cours [Gestion de projets - Outils collaboratifs - Gitlab et git](https://pole-calcul-formation.gricad-pages.univ-grenoble-alpes.fr/outils-collaboratifs-gitlab/cours/)

Les modèles de fichiers yml à utiliser au cours de la démo sont disponibles dans le répertoire [templates](./templates).


## Objectifs


Mise en situation : considérant un code de calcul, nous allons montrer comment utiliser la CI pour les différentes étapes du cycle de vie de ce logiciel.

Les besoins : 

* Configure, build et test d'un logiciel (cmake, make, make test) 
* Pour différents systèmes d’exploitation (ubuntu, debian ...)
* Pour différentes configurations (Debug, release ...)
* Générer de la documentation et publier la page web du logiciel
* Contrôler et automatiser des publications de versions, de releases
* Le tout en collaborant entre développeurs et avec des utilisateurs
* ...

Le répertoire courant contient :

- du code source (C++, Fortran, Python) dans les répertoires python et src
- des tests décrits dans les langages ci-dessus
- des fichiers de conf CMake et un CMakeLists.txt (pour en savoir plus à ce sujet, voir par exemple notre cours [Des sources à l'exécutable : la chaine de compilation](https://pole-calcul-formation.gricad-pages.univ-grenoble-alpes.fr/ced/plans_modules/))

Bref, tout ce qui est nécessaire pour configurer, construire, tester et installer un code.
Inutile de vous attarder sur le contenu précis de ces fichiers. Ce qu'il faut retenir est que pour pouvoir utiliser ce code, il faut :

* une étape de configuration (via CMake) dont le rôle est de vérifier la présence d'un compilateur, de dépendances du code etc
* une étape de compilation/édition de lien pour créer un exécutable et des tests (make)
* une étape de test (make test)

Par exemple, sur votre laptop, pour lancer la séquence complète (conf/build/test)

```bash
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:pole-calcul-formation/outils-collaboratifs-gitlab/demos/ci-example-advanced.git 
# 1 Configuration
cmake -S ci-example-advanced -B build -DCONF_FILE=ci-example-advanced/configs/default.cmake
# 2 Build
cmake --build build
# 3 Test
ctest --test-dir build --output-junit test_results.xml
```
Les fichiers dans le répertoire [configs](./configs) contiennent la description (pour cmake) des différents jeux de paramètres pour la configuration du code.

## Un premier pipeline 

Première étape : déléguer à la CI les étapes 1 à 3 ci-dessus, sur un système Ubuntu, pour un jeu de paramètres par défaut

Le modèle : [templates/demo2.1.yml](./templates/demo2.1.yml) avec

* un job configure, pour l'étape 1,  résultats dans un répertoire temporaire build, à conserver pour l'étape suivante
* un job build, qui reprend le résultat du configure et lance la compilation
* un job test, qui reprend les résultats de la compilation/édition de lien et exécute des tests


#### Commentaires

* **before_script** En début de fichiers yml : des actions à exécuter pour tous les jobs.
Utilisé ici pour installer les dépendances de notre code, dans chaque container Docker.

Il est également possible d'utiliser before_script au niveau du job, pour spécialiser celui-ci.

* Le mot-clé 'needs' défini une dépendance entre les jobs.

## Version plus complète avec plusieurs OS et des configs différentes

Le modèle : [templates/demo2.2.yml](./templates/demo2.2.yml) avec

- une séquence ubuntu, paramètres de default.cmake
- une séquence ubuntu, paramètres de minimal.cmake
- une séquence debian, paramètres de full_debug.cmake


On utilise maintenant un script [build_soft.sh](./scripts/build_soft.sh) pour gérer chaque étape.
Ce script a besoin des variables :

- BUILD_MODE : configure, build ou test, pour définir le comportement de la commande cmake
- CONF_FILE : pour choisir un jeu de paramètres

:bulb: une variable définie dans un job sera ensuite disponible dans le container Docker, comme une variable d'environnement.

Le pipeline a maintenant trois étages et trois "files" indépendantes. 

## Un pas de plus : utilisation de templates

Les modèles : [templates/demo2.3.yml](./templates/demo2.3.yml) et [templates/tmpl2.3.yml](./templates/tmpl2.3.yml).

Pour simplifier la rédaction du yaml, il est possible d'utiliser des templates, pour par exemple factoriser des opérations.

On va créer un fichier ci/ci-templates.yml, qui sera inclus dans le .gitlab.yml

```yaml
include:
   - ci/ci-templates.yml
```

Ce fichier contiendra la description des templates, des modèles réutilisables dans plusieurs jbs.
Par exemple :

```yaml
.script_artif:
  script:
    - env
    - sh scripts/build_soft.sh
  artifacts:
      paths:
        - build/
      expire_in: 2 days 
```

qui peut être utilisé dans des jobs via le mot-clé **extends**

```yaml
ubuntu:configure:
  image: ubuntu:22.04
  variables:
    CONF_FILE: configs/full_debug.cmake
  extends:
    - .script_artif
```

Tout ce qui est décrit dans script_artif sera appliqué à ubuntu:configure

Utilisez le fichier [templates/tmpl2.3.yml](./templates/tmpl2.3.yml) pour remplir ci/ci-templates.yml et [templates/demo2.3.yml](./templates/demo2.3.yml) pour
.gitlab-ci.yml

Grâce aux templates, le fichier .gitlab-ci.yml est beaucoup plus lisible et l'écriture de nouveaux jobs plus simple.
Notez également au passage l'utilisation du mot-clé **reports** dans les artifacts pour l'affichage des tests.

```yaml
  artifacts:
      paths:
        - build/
      expire_in: 3 days 
      reports:
        junit: $BUILD_DIR/test_results.xml
```
Cela permet d'exploiter les sorties junits en xml et de les formater proprement sur la page du pipeline.
Voir l'onglet Tests qui apparaitra sur cette page.


## Utilisation de gitlab registry et création automatique d'images docker

Dans l'exemple ci-dessus, à chaque push, les images docker sont chargées, démarrées et mise à jour (apt update et cie). Ceci n'est pas forcément utile et prend du temps et des ressources, alors que l'objectif est avant tout de tester le code, pas l'installation de package Ubuntu ou Debian.

Nous allons voir qu'il est possible d'utiliser l'intégration continue pour :

- construire une image docker,
- la sauvegarder dans le projet
- puis l'utiliser comme image pour d'autres jobs.

Modèles : [templates/tmpl2.4.yml](./templates/tmpl2.4.yml) et  [templates/demo2.4.yml](./templates/demo2.4.yml).

Trois étapes :

1. Activer les registries dans le projet => emplacement de sauvegarde pour les images Docker
2. Ecrire un Dockerfile
2. Créer un job qui va générer une image docker à partir du Dockerfile et la sauvegarder dans les registries

##### Activation des registries

Projet->Settings->General->Visibility, project features, permissions, cocher 'Container Registries'

##### Dockerfile

Des exemples sont disponibles dans le répertoire [ci/dockerfiles](./ci/dockerfiles)

Par exemple, pour créer une image, basée sur ubuntu avec les dépendances nécessaires pour notre soft

```
FROM ubuntu:22.04
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone && apt update  && apt upgrade -y && apt install -y -qq \
    cmake \
    git-core \
    make \
	g++ \
    gfortran \
	libboost-dev \
	libfftw3-dev \
	python3 \
	python3-pip \
	python3.10-venv	\
	doxygen  \
	graphviz
RUN python3 -m pip install sphinx
RUN apt autoclean -y && apt autoremove -y&& rm -rf /var/lib/apt/lists/*
```

##### Job CI 

Pour créér des images Docker dans un job CI, nous allons utiliser un outil nommé Kaniko (voir [Use kaniko to build Docker images](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html))

Un template nommé .docker-create-image est disponible dans le fichier [templates/tmpl2.4.yml](./templates/tmpl2.4.yml).

Nous pouvons utilisé ce template pour créer des jobs en charge de construire les images Docker. Voir [templates/demo2.4.yml](./templates/demo2.4.yml)

Inutile d'entrer dans les détails de ce jobs. Il faut simplement retenir que :

* Nous utilisons l'outil kaniko pour construire et pousser une image dans le registry du projet. Voir [gitlab et kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)
* Il faut préciser le chemin (relatif) vers le dockerfile, après l'option '--dockerfile'
* Il faut préciser le nom de l'image à créer après l'option '--destination'
* Nous avons ajouter une règle précisant que ce job ne doit être exécuté que si le message commence par [docker-build]

Le reste est du copié-collé de la doc !

Ensuite, il sera possible d'utiliser cette image pour certains jobs ce qui évitera l'appel à "apt install" à chaque exécution.

##### Exécution ...

Après avoir modifié vos fichiers, commitez en commençant votre message par [docker-build] (une règle a été ajoutée, voir le template !) et vérifiez le pipeline de CI.

Si le job se termine, vous devriez :

- retrouver votre image docker dans les registries du projet
- pouvoir vérifier que les jobs 'ubuntu' ont bien tourné sur cette image.


Via les jobs d'intégration continue ci-dessus, nous avons :

- Créé et sauvegardé des images Docker,
- Compilé le code sur plusieurs OS,

## Contrôle du workflow

Modèle [templates/tmpl2.5.yml](./templates/tmpl2.5.yml)

Dans ce template, nous définissons un workflow complexe, afin de différencier les comportements de la CI en fonction de la branche visée, des merge-requests etc.

Voir la section 

```yaml
workflow:
  rules:
...
```

## Mise à disposition d'une image 

Modèle [templates/tmpl2.6.yml](./templates/tmpl2.6.yml)

templates .soft-install et .notebook-install

Dans ces templates, nous utilisons pour construire automatiquement des images Docker, les fichiers suivants :

- [softlab/Dockerfile](ci/dockerfiles/softlab/Dockerfile) : utilise la variable IMAGENAME comme image source, installe notre logiciel (via cmake --install), à partir d'un build dans un job précédent, puis génère une image docker dans les registries d'un AUTRE projet.
Pour cette étape il est nécessaire de générer un token de groupe et de l'utiliser dans la CI (voir les détails dans le cours)

- [pylab/Dockerfile](ci/dockerfiles/pylab/Dockerfile) : basé sur une image python/notebook, installation du paquet python décrit dans le répertoire [python](./python) 

Ces templates sont utilisés dans les jobs ubuntu22.04:install et python:install du modèle [demo2.6.yml](./templates/demo2.6.yml)

Exemple :
```yaml
ubuntu22.04:install:
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE/ubuntu22.04
    RESULT_NAME: $CI_REGISTRY/pole-calcul-formation/outils-collaboratifs-gitlab/demos/ci-example-trigger/mysoft-ubuntu
  extends:
    - .soft-install
    - .devel-rules
  needs: ["ubuntu:build"]
```

A vous de tester ...

## Release

Il est possible de déclencher la construction d'une Release dès qu'un tag est associé au projet.

Voir le job *release_job* dans [demo2.8.yml](./templates/demo2.8.yml)

Dès qu'un tag sera associé au repo git, via l'interface ou via un git tag suivi d'un push,
une Release sera construite et disponible via le menu [Deploy->Releases](https://gricad-gitlab.univ-grenoble-alpes.fr/pole-calcul-formation/outils-collaboratifs-gitlab/demos/ci-example-advanced/-/releases) du projet.

## Cross-project

Il est possible, à partir d'un job, de déclencher la CI dans un sous répertoire ou même dans un autre projet.

On utilisera pour cela le mot clé **trigger**.

Deux exemples sont disponibles dans [demo2.8.yml](./templates/demo2.8.yml)

- Le job generate-doc qui va exécuter le contenu de [docs/build-doc.yml](./docs/build-doc.yml), pour générer la doc du code

- Le job bridge:example qui va déclencher la CI dans le projet [demos/ci-example-trigger](https://gricad-gitlab.univ-grenoble-alpes.fr/pole-calcul-formation/outils-collaboratifs-gitlab/demos/ci-example-trigger)
Dans ce projet : 

  - on utilise une image construite par un job de [Exemples intégration continue - CI CD pour un code de calcul]  (https://gricad-gitlab.univ-grenoble-alpes.fr/pole-calcul-formation/outils-collaboratifs-gitlab/demos/ci-example-advanced/), le job ubuntu22.04:install.
  - cette image est sauvée dans les registries du projet ci-example-trigger
  - on exécute le code installé dans cette image



<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

