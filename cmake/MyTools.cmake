#
# Some convenience macros
#
include(CMakePrintHelpers)


# Collect files from a list of directories
#
# Usage:
#
# collect_files(VAR <output variable name> DIRS <dirs list> EXTS <extensions list> EXCLUDE <files list> RECURSIVE)
#
# Result : set (parent scope) VAR with files in
# dirs from DIRS matching file extension in EXTS
# Set RECURSIVE to true to search recursively.
# Remarks:
# - dir in DIRS are relative to CMAKE_CURRENT_SOURCE_DIR
#
function(collect_files)

  set(oneValueArgs VAR)
  set(multiValueArgs DIRS EXTS)
  set(options RECURSIVE)
  cmake_parse_arguments(collect "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  # Scan all dirs and check all exts ...

  foreach(DIR IN LISTS collect_DIRS)
    foreach(_EXT IN LISTS collect_EXTS)
      if(collect_RECURSIVE)
	file(GLOB_RECURSE FILES_LIST
          RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} CONFIGURE_DEPENDS
          ${DIR}/*.${_EXT})
      else()
	file(GLOB FILES_LIST
          RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} CONFIGURE_DEPENDS
          ${DIR}/*.${_EXT})
      endif()
      if(FILES_LIST)
	list(APPEND COLLECTION ${FILES_LIST})
      endif()
    endforeach()
  endforeach()
  if(COLLECTION)
    list(LENGTH COLLECTION _FILES_LEN)
    if (_FILES_LEN GREATER 1)
      list(REMOVE_DUPLICATES COLLECTION)
    endif()
  endif()
  set(${collect_VAR} ${COLLECTION} PARENT_SCOPE)

endfunction()

# Collect source files.
#
# Usage:
#
# get_sources(VAR outputvar-name DIRS <dirs list> EXCLUDE <files list>)
#
# Result : set (parent scope) <COMPONENT>_SRCS with files in
# dir1, dir2 matching standard extension for C,C++ and Fortran.
# Do not include files listed after EXCLUDE option.
#
# Remarks:
# - dir1, dir2 ... are relative to CMAKE_CURRENT_SOURCE_DIR
#
function(get_sources)

  set(oneValueArgs VAR) # output variable name
  set(multiValueArgs DIRS EXCLUDE)
  cmake_parse_arguments(source "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  # Get list of extensions to be taken into account
  foreach(_EXT
      ${CMAKE_CXX_SOURCE_FILE_EXTENSIONS}
      ${CMAKE_C_SOURCE_FILE_EXTENSIONS}
      ${CMAKE_Fortran_SOURCE_FILE_EXTENSIONS})
    list(APPEND SRC_EXTS ${_EXT})
  endforeach()
  list(REMOVE_DUPLICATES SRC_EXTS)
  
  collect_files(VAR SOURCE_FILES DIRS ${source_DIRS} EXTS ${SRC_EXTS})

  # Check if some sources are to be excluded from build
  foreach(_FILE IN LISTS source_EXCLUDE)
    list(REMOVE_ITEM SOURCES_FILES ${_FILE})
  endforeach()

  set(${source_VAR} ${SOURCE_FILES} PARENT_SCOPE)

endfunction()

